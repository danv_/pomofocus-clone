# Pomofocus 'clone'

Check out the live version:  
https://pomofocus-remake.netlify.app/

## Resources

### Colors

main - #fff  
background - #862e9c  
content bg - rgba(255, 255, 255, 0.3)  
shadow - #6b257d  
highlight - rgba(255, 255, 255, 0.8)  

### Icons
 - https://phosphoricons.com/
