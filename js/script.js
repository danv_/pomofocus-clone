// increase decrease functionality
let pomodoroValue = 1;

const btnPomUp = document.querySelector(".btn--up");
const btnPomDown = document.querySelector(".btn--down");

btnPomUp.addEventListener("click", function () {
  pomodoroValue++;
  document.getElementById("task-new-est-pomodoros").value = pomodoroValue;
});

btnPomDown.addEventListener("click", function () {
  pomodoroValue--;
  document.getElementById("task-new-est-pomodoros").value = pomodoroValue;
});

//show hide task list
const btnListToggle = document.querySelector(".btn-task-list-toggle");
const containerEl = document.querySelector(".task-list");

btnListToggle.addEventListener("click", function () {
  containerEl.classList.toggle("task-list-open");
});

//show hide new task
const btnAddTask = document.querySelector(".btn-add-task");
const btnAddCancel = document.querySelector(".btn-task-cancel");
const taskAddEl = document.querySelector(".tasks");

btnAddTask.addEventListener("click", function () {
  console.log("yeet");
  taskAddEl.classList.add("add-open");
});

btnAddCancel.addEventListener("click", function () {
  console.log("yeet");
  taskAddEl.classList.remove("add-open");
});
